# LANA - LiDAR Assisted NAvigator

Project that uses LiDAR information with GPS to navigate a robot in a
wild, wild environment.

Developed as the assignment for the SSC0712 subject, at ICMC-USP.

## Setup and usage

1. As a requirement, Python must be set up in webots in order to work.
2. Simply clone into the `controllers/` directory in your Webots world.
3. `lidavoider.py` is a webots controller, you must attach it to a robot in the simulator that is configured for the python environment.
4. Run your simulation!

## Demonstrations

The progress is being documented in the following youtube playlist: https://www.youtube.com/playlist?list=PL5XBYkzIUBiuHxkJvxM3JgrYICQdGXDP2
