import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import threading
from multiprocessing import Process, Lock, Queue
import time

runAll = True

"""
r = (0, 0, 0)
def get_readings():
    global r
    global runAll
    with open('readings', 'r') as f:
        while runAll:
            l = f.readline()
            try:
                x, y, ori = l.split()
            except:
                continue
            x = float(x)
            y = float(y)
            ori = float(ori)
            #print('Pos', x, y, ori)
            r = x, y, ori

def get_grid():
    global grid_map
    global runAll
    with open('grid', 'r') as f:
        while runAll:
            l = f.readline()
            try:
                x, y = l.split()
            except:
                continue
            x = int(x) + grid_map.shape[0] // 2
            y = int(y) + grid_map.shape[1] // 2
            #print('Grid', x, y)
            grid_map[x, y] = 255

def get_lidar():
    global lidar_map
    global runAll
    with open('lidar', 'r') as f:
        while runAll:
            l = f.readline()
            #print('[lidar]:', l)
            try:
                x, y = l.split()
                x = int(float(x) / 0.1) + lidar_map.shape[0] // 2
                y = int(float(y) / 0.1) + lidar_map.shape[1] // 2
                x = max(min(x, lidar_size), 0)
                y = max(min(y, lidar_size), 0)
                lidar_map[x, y] = 255
            except Exception as e:
                #print('[lidar]:', e)
                continue
"""

lidar_size = 1000

# Transforms real coordinates into plot coordinates
def coordinates_to_map(x, y):
    x = int(float(x) / 0.1) + lidar_size // 2
    y = int(float(y) / 0.1) + lidar_size // 2
    x = max(min(x, lidar_size), 0)
    y = max(min(y, lidar_size), 0)
    y = lidar_size - y
    return (y, x)

# q - queue
# p - pipe
def get_data(q):
    runAll = True
    with open('connector', 'r') as f:
        print('opened file')
        while runAll:
            l = f.readline()
            if l.startswith('M'): # Mark location
                # TODO: remove M and space
                l = l[2:]
                try:
                    x, y = l.split()
                except:
                    continue
                #print('Grid', x, y)
                #grid_map[x, y] = 255
                q.put(['M', coordinates_to_map(x, y)])
            elif l.startswith('R'): # Readings
                l = l[2:]
                try:
                    x, y, ori = l.split()
                    x = float(x)
                    y = float(y)
                    ori = float(ori)
                except:
                    continue

                #print('Pos', x, y, ori)
                r = (x, y, ori)
                q.put(['R', r])
            else:
                #print('[lidar]:', l)
                try:
                    x, y = l.split()
                    q.put(['L', coordinates_to_map(x, y)])
                    lidar_map[x, y] = 255
                except Exception as e:
                    #print('[lidar]:', e)
                    continue

def get_values_from_queue(comm_q, lidar_q):
    lidar_map2 = np.zeros((lidar_size, lidar_size))
    while True:
        if comm_q.empty():
            time.sleep(0.1)
            continue
        while not comm_q.empty():
            try:
                upd = comm_q.get()
                #print('Got: ', upd)
                if upd[0] == 'M':
                    x, y = upd[1]
                    grid_map[x, y] = 255
                elif upd[0] == 'L':
                    x, y = upd[1]
                    lidar_map2[x, y] = 255
                #elif upd[0] == 'R':
                #    r = upd[1]
                #    append_to_plot(r)
            except Exception as e:
                print(upd)
                print(e)
        temp = np.copy(lidar_map2)
        lidar_q.put(temp)

def main():
    comm_q = Queue()
    getter_process = Process(target=get_data, args=(comm_q,))
    getter_process.start()

    lidar_q = Queue()
    lidar_process = Process(target=get_values_from_queue, args=(comm_q, lidar_q))
    lidar_process.start()

    # Set up the figure and axis
    fig, (ax, ax2, ax3) = plt.subplots(ncols=3, figsize=(16, 5))

    var_num = 3
    # Storing sensor readings
    readings = [np.zeros(100) for _ in range(var_num)]

    global lidar_map
    global grid_map
    global lidar_plot

    # Transform the coordinates to 10x. Better view I guess
    grid_map = np.zeros((lidar_size, lidar_size))
    lidar_map = np.zeros((lidar_size, lidar_size))

    lines = []
    map_plot = ax2.imshow(grid_map)
    lidar_plot = ax3.imshow(lidar_map)

    # Rolls the array and adds the new value into the most recent (last)
    # position
    def append_to_plot(new_data):
        for i, r in enumerate(readings):
            r = np.roll(r, -1)
            r[-1] = new_data[i]
            lines[i].set_ydata(r)
            readings[i] = r

    # Initialize the plot elements
    for r in readings:
        line, = ax.plot(r)
        lines.append(line)

    def init():
        map_plot.set_array(grid_map)
        lidar_plot.set_array(lidar_map)
        for i, r in enumerate(readings):
            lines[i].set_ydata(r)
        return tuple(lines + [map_plot, lidar_plot])

    def update(i):
        global lidar_map
        global lidar_plot

        if not lidar_q.empty():
            lidar_map = lidar_q.get()
            #map_plot = ax2.imshow(grid_map)
            lidar_plot = ax3.imshow(lidar_map)
        #ax.relim()
        #ax.autoscale_view()
        return tuple(lines + [map_plot, lidar_plot])
    try:
        anim = animation.FuncAnimation(fig, update, frames=None, init_func=init, interval=50)
        anim._start()
        plt.show()
    except KeyboardInterrupt:
        print('aiaiaiaiaiaiai')
        exit()
    print('Bye!')

if __name__ == '__main__':
    main()
