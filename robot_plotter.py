import numpy as np
import matplotlib.pyplot as plt

lidar_size = 400
map_scale = 0.2

# Transforms real coordinates into plot coordinates
# def coordinates_to_map(x, y):
#     x = int(float(x) / map_scale) + lidar_size // 2
#     y = int(float(y) / map_scale) + lidar_size // 2
#     x = max(min(x, lidar_size), 0)
#     y = max(min(y, lidar_size), 0)
#     y = lidar_size - y
#     return (y, x)

class RobotPlotter():
    def __init__(self, axes, robot = None):
        self.axes = axes
        
        # Lidar
        axes[0].set_xlim(0, lidar_size)
        axes[0].set_ylim(0, lidar_size)
        
        # Bounce
        axes[1].set_xlim(0, lidar_size)
        axes[1].set_ylim(0, lidar_size)

        self.robot = robot
        self.lidar_plot = self.axes[0].imshow(self.robot.grid_map, cmap='gray', vmin=0, vmax=1)
        self.bounce_plot = self.axes[1].imshow(self.robot.bounce_map, cmap='gray', vmin=0, vmax=1)

    def __call__(self, i):
        a = np.copy(self.robot.grid_map)
        # rx, ry = coordinates_to_map(self.robot.x, self.robot.y)
        # Mark 
        self.lidar_plot.set_array(self.robot.grid_map)
        self.bounce_plot.set_array(self.robot.bounce_map)
        return self.lidar_plot, self.bounce_plot

    def set_data(self):
        pass
