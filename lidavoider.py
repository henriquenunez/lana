print('------------\nInitializing Controller')

import time
import random
import math
from controller import Robot, Motor, GPS, Lidar
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import threading
from multiprocessing import Process

from robot_plotter import RobotPlotter, lidar_size, map_scale

output_things = False

#matplotlib.use('Agg')
#time.sleep(10)
lidar_readings = np.zeros(100)

# Lidar cleaner
from skimage.morphology import disk, opening, closing, dilation
close_kernel = disk(5)
astar_kernel = disk(3)
small_close_kernel = disk(2)
#print(close_kernel)
open_kernel = disk(1)
#print(open_kernel)

# Create the robot instance
robot = Robot()

# Get references to the Pioneer's motors
front_left_motor = robot.getDevice('front left wheel')
front_right_motor = robot.getDevice('front right wheel')
rear_left_motor = robot.getDevice('back left wheel')
rear_right_motor = robot.getDevice('back right wheel')

front_left_motor.setPosition(float('inf'))
front_right_motor.setPosition(float('inf'))
rear_left_motor.setPosition(float('inf'))

rear_right_motor.setPosition(float('inf'))
# Get references to the GPS and LiDAR sensors
gps = robot.getDevice('gps')
compass = robot.getDevice('compass')
lidar = robot.getDevice('Sick S300')

## Enable the sensors
gps.enable(64)
compass.enable(64)
lidar.enable(64)

# Set the maximum motor velocity
max_velocity = front_left_motor.getMaxVelocity()

# 10cm resolution grid
grid_size = (200, 200) # meters

def mini_viewer():
    print('mini viewer')
    fig, ax = plt.figure()
    plt.plot([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    plt.show()
    time.sleep(20)

# Navigation
import numpy as np
import heapq

def point_map_to_world(x, y):
    x = (x - lidar_size // 2) * map_scale
    y = (y - lidar_size // 2) * map_scale
    return y, x

def point_world_to_map(x, y):
    x = int(float(x) / map_scale) + lidar_size // 2
    y = int(float(y) / map_scale) + lidar_size // 2
    x = max(min(x, lidar_size), 0)
    y = max(min(y, lidar_size), 0)
    return y, x

def heuristic(point, goal):
    # Calculate the Manhattan distance between two points
    return abs(point[0] - goal[0]) + abs(point[1] - goal[1])

def get_neighbours(point, grid):
    # Get the valid neighbouring cells of a point in the grid
    neighbours = []
    rows, cols = grid.shape
    x, y = point

    # Possible movement directions: up, down, left, right
    directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    for dx, dy in directions:
        nx, ny = x + dx, y + dy

        if nx >= 0 and nx < rows and ny >= 0 and ny < cols and grid[nx, ny] == 0:
            neighbours.append((nx, ny))

    return neighbours

def reconstruct_path(came_from, current):
    # Reconstruct the path from the start point to the current point
    path = []

    while current in came_from:
        path.append(current)
        current = came_from[current]

    path.append(current)
    return path[::-1]

def A_star(grid, start, goal):

    grid = dilation(grid, footprint=astar_kernel)

    rows, cols = grid.shape
    open_list = []
    came_from = {}
    g_scores = {start: 0}
    f_scores = {start: heuristic(start, goal)}

    heapq.heappush(open_list, (f_scores[start], start))

    while open_list:
        _, current = heapq.heappop(open_list)

        if current == goal:
            # Goal reached, reconstruct and return the path
            return reconstruct_path(came_from, current)

        neighbours = get_neighbours(current, grid)

        for neighbour in neighbours:
            g_score = g_scores[current] + 1  # Assuming a constant cost of movement

            if neighbour not in g_scores or g_score < g_scores[neighbour]:
                # Update the g_score and f_score for the neighbour
                g_scores[neighbour] = g_score
                f_scores[neighbour] = g_score + heuristic(neighbour, goal)
                came_from[neighbour] = current
                
                if neighbour not in [item[1] for item in open_list]:
                    # Add the neighbour to the open list for exploration
                    heapq.heappush(open_list, (f_scores[neighbour], neighbour))
    
    # No path found
    return None

"""
- Calculate the direction vector from one point to another
- Orients the robot
"""
def go_to(x, y, xp, yp):
    vx, vy = xp-x, yp-y
    rotateLeft()

def rotateLeft():
    front_left_motor.setVelocity(-max_velocity)
    front_right_motor.setVelocity(max_velocity)
    rear_left_motor.setVelocity(-max_velocity)
    rear_right_motor.setVelocity(max_velocity)

def increaseTheta():
    _velocity = max_velocity / 10.0
    front_left_motor.setVelocity(-_velocity)
    front_right_motor.setVelocity(_velocity)
    rear_left_motor.setVelocity(-_velocity)
    rear_right_motor.setVelocity(_velocity)

def decreaseTheta():
    _velocity = max_velocity / 10.0
    front_left_motor.setVelocity(_velocity)
    front_right_motor.setVelocity(-_velocity)
    rear_left_motor.setVelocity(_velocity)
    rear_right_motor.setVelocity(-_velocity)

rotating_left = 0
def go_back():
    front_left_motor.setVelocity(-max_velocity)
    front_right_motor.setVelocity(-max_velocity)
    rear_left_motor.setVelocity(-max_velocity)
    rear_right_motor.setVelocity(-max_velocity)

def commonSpeed(vel):
    front_left_motor.setVelocity(vel)
    front_right_motor.setVelocity(vel)
    rear_left_motor.setVelocity(vel)
    rear_right_motor.setVelocity(vel)

class RobotLogic:
    def __init__(self):
        self.modcounter = 0
        self.op = 'STOP'
        self.should_scan = True
        self.grid_map = np.zeros((lidar_size, lidar_size))
        self.bounce_map = np.zeros((lidar_size, lidar_size))
        # print('opening file')
        if output_things:
            self.f = open('/Users/hiram/Workspace/obstacle_avoidance/controllers/lidavoider/connector', 'w')
        self.astarcounter = 0
        print('Robot initialized')
        self.clean_lidar_counter = 100

    #def __del__(self):
    #    self.f.close()
    """
    Returns the angle to which points need to be rotated to be in world
    coordinates.
    """
    def angleRobotToWorld(self):
        x = self.ori[0]
        y = self.ori[1]
        theta = math.acos(y)
        if x < 0:
            theta = 2 * math.pi - theta
        return theta

    def transform_lidar(self):
        lidar_to_map = np.zeros((540, 2))
        for i, d in enumerate(self.lidar_data):
            theta = 135 - 0.5 * i
            theta = math.radians(theta)
            # Position relative to robot
            pos_r = [d * math.cos(theta), d * math.sin(theta)]
            lidar_to_map[i, :] = np.array(pos_r)

        # Now compute the position relative to the world
        # Rotate against the robot's orientation
        rtw_ang = self.angleRobotToWorld()
        rotMat = np.array([[np.cos(rtw_ang), -np.sin(rtw_ang)],
                           [np.sin(rtw_ang),  np.cos(rtw_ang)]])
        lidar_to_map = lidar_to_map.dot(rotMat.T)
        # Add the translation
        # We ae changing basis, so the robot's location needs to be added to the
        # relative location of the points
        # NOTE: the y coordinate has been put to minus because of the map
        # implementation, but this is incorrect. TODO: pass it to +y
        trans = np.array([self.x, self.y])
        lidar_to_map += trans

        self.lidar_to_map = lidar_to_map

    def write_sensor_data(self, x, y, ori):
        l = 'R {} {} {}'.format(x, y, ori[0])
        #print(l)
        if output_things: self.f.write(l)

    def getSensors(self):
        # Read sensor data
        self.gps_data = gps.getValues()
        self.lidar_data = lidar.getRangeImage()
        self.compass_data = compass.getValues()

        #front_dist = lidar_data[len(lidar_data) // 2]
        #self.front_dist = np.min(self.lidar_data)

        x = self.gps_data[0]
        y = self.gps_data[1]
        ori = self.compass_data
        #print('x: {} y: {} orientation: {}'.format(x, y, ori))
        #print('orientation: {}'.format(ori))

        self.write_sensor_data(x, y, ori)

        self.x = x
        self.y = y
        self.ori = ori

    def gotoTarget(self, x, y):
        print('going to:', x, y)
        self.target_x = x
        self.target_y = y
        dx = x - self.x
        dy = y - self.y
        norm = math.sqrt(dx ** 2 + dy ** 2)
        dx /= norm
        dy /= norm
        angle = math.acos(dx)
        if dy < 0:
            angle = 2 * math.pi - angle
        self.target_angle = angle
        self.op = 'GOTO'

    def closeEnough(self):
        # print('target: ', (self.target_x, self.target_y), 'current: ', (self.x,\
        #                                                                 self.y))
        return math.sqrt(pow(self.target_x - self.x, 2) + pow(self.target_y - self.y, 2)) < 0.5

    def markCurrentLocation(self):
        x, y = point_world_to_map(self.x, self.y)
        l = 'M {} {}'.format(x, y)
        if output_things: self.f.write(l)
        # self.bounce_map[x, y] = 1

    def markLidar(self):
        acc = ''
        for x, y in self.lidar_to_map:
            # print(x, y)
            try:
                #TODO: check isnan and isinf instead of catching exceptions...
                _x, _y = point_world_to_map(x, y)
                self.grid_map[_x, _y] = 1
                acc += '{} {}\n'.format(x, y)
            except Exception as e:
                continue
                # print(e)
        if output_things: self.f.write(acc)

    def cleanLidar(self):
        # print('Cleaning')
        #self.grid_map = closing(opening(self.grid_map, disk_kernel), disk_kernel)
        # opening(self.grid_map, footprint=open_kernel, out=self.grid_map)
        # closing(self.grid_map, footprint=close_kernel, out=self.grid_map)

        _map = self.grid_map
        _map = closing(_map, footprint=small_close_kernel)
        _map = opening(_map, footprint=open_kernel)
        _map = closing(_map, footprint=close_kernel)
        # closed_map = closing(closed_map, close_kernel)
        self.grid_map = _map

    def fill_bounce_map(self, path):
        for pt in path:
            # print('Path: ', pt)
            self.bounce_map[pt[0], pt[1]] = 1

    def controlLogic(self):
        #print('Modcounter: ', self.modcounter)

        self.getSensors()
        if self.should_scan:
            self.transform_lidar()
            self.markLidar()

        if self.clean_lidar_counter == 0:
            self.cleanLidar()
            self.clean_lidar_counter = 140
        else:
            self.clean_lidar_counter -= 1

        # print(self.op)
        if self.op == 'STOP':
            commonSpeed(0)
        elif self.op == 'TURN':
            if self.modcounter <= 0:
                self.op = 'FW'
            rotateLeft()

        elif self.op == 'A_star_TURN':
            if self.modcounter <= 0:
                self.op = 'A_star_FW'
            rotateLeft()

        elif self.op == 'GOTO':
            # Check if the position was reached
            if self.closeEnough():
                self.op = 'STOP'
            angle = self.angleRobotToWorld()
            # Make the robot rotate until a predefined position
            if abs(angle - self.target_angle) > 0.02:
                # print('Angle: ', angle, ' target:', self.target_angle)
                if angle > self.target_angle:
                    decreaseTheta()
                    return
                else:
                    increaseTheta()
                    return
            # Else just keep going forward
            commonSpeed(max_velocity)
        elif self.op == 'FW':
            # Move the robot forward
            commonSpeed(max_velocity)
            front_dist = np.min(self.lidar_data)
            if front_dist < 0.5: #+ 0.1 * random.random():
                self.markCurrentLocation()
                self.op = 'TURN'
                self.modcounter = 20 * random.random()

        elif self.op == 'A_star_FW':
            # Move the robot forward
            if self.fw_timeout < 0:
                self.op = 'A_star'

            commonSpeed(max_velocity)
            front_dist = np.min(self.lidar_data)
            if front_dist < 0.3: #+ 0.1 * random.random():
                self.op = 'A_star_TURN'
                self.modcounter = 20 * random.random()

            self.fw_timeout -= 1

        elif self.op == 'ALIGN_NORTH':
            pass
        
        elif self.op == 'A_star':
            print('Astar')
            self.should_scan = False
            if self.astarcounter <= 0: # interval between readings
                print('Running astar')
                self.astarcounter = 100
                start = point_world_to_map(self.x, self.y)
                final = point_world_to_map(self.final_pos[0], self.final_pos[1])
                _path = A_star(self.grid_map, start, final) # Update path based on map and current location
                self.bounce_map = np.zeros((lidar_size, lidar_size))
                self.path_index = 0
                if not _path:
                    print('Path failed!')
                    #self.op = 'STOP'
                    self.grid_map = np.zeros(self.grid_map.shape)
                    self.should_scan = True
                    self.op = 'A_star_FW'
                    self.astarcounter = 0
                    self.fw_timeout = 900
                    return
                self.fill_bounce_map(_path)
                self._path = _path

            # Goto next suggested position.
            next_pos = self._path[self.path_index]
            self.path_index += 1
            _x, _y = point_map_to_world(next_pos[0], next_pos[1])
            self.astarcounter -= 1
            self.gotoTarget(_x, _y)
            print('Next position: ', next_pos, 'xy: ', _x, _y)
            self.op = 'GOTO_A_star'

        elif self.op == 'GOTO_A_star':
            """
            I know that I can do better. I was just hurrying...
            """
            front_dist = np.min(self.lidar_data)
            if front_dist < 0.3:
                print('Hit A*')
                self.should_scan = True
                self.op = 'A_star_FW'
                self.fw_timeout = 800
                self.astarcounter = 0
            # Check if the position was reached
            if self.closeEnough():
                print('Close enough! Going back to A*')
                self.op = 'A_star'
            angle = self.angleRobotToWorld()
            # Make the robot rotate until a predefined position
            if abs(angle - self.target_angle) > 0.01:
                # print('Angle: ', angle, ' target:', self.target_angle)
                if angle > self.target_angle:
                    decreaseTheta()
                    return
                else:
                    increaseTheta()
                    return
            # Else just keep going forward
            commonSpeed(max_velocity)
        else: # Failsafe
            self.op = 'STOP'

        if self.modcounter > 0:
            self.modcounter -= 1
def print_grid():
    for l in grid_map:
        print(l)

def main():
    rl = RobotLogic()

    fig, axes = plt.subplots(1, 2)
    plotter = RobotPlotter(axes, rl)
    anim = animation.FuncAnimation(fig, plotter, frames=None, blit=True)

    # Set an interactive backend (e.g., for Jupyter Notebook)
    plt.ion()
    plt.show(block=False)

    # Tree pot location
    rl.final_pos = (25, 25)

    initial_mapping = 2000
    
    rl.op = 'FW'
    # Main control loop
    plt.pause(0.001)
    while robot.step(64) != -1:
        rl.controlLogic()
        # if rl.op == 'STOP':
        #     #rl.gotoTarget(10, 0)
        #     rl.gotoTarget(random.random() * 5 - 2.5, random.random() * 5 - 2.5)
        plt.pause(0.001)
        if initial_mapping > 0:
            initial_mapping -= 1
            if initial_mapping == 0:
                rl.op = 'A_star'

main()
